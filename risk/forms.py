from pickle import NONE
from bootstrap_modal_forms.forms import BSModalModelForm
from django.forms import DateTimeInput, HiddenInput
from django import forms

from users.utils import (get_customers_qs, get_phase_manager_qs,
                         get_project_managers_qs)

from .models import LFA, Issue, Project, ProjectPhase, Risk, Task, Task


class CreateProjectForm(BSModalModelForm):

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)
        print(get_customers_qs(company=user.company))
        self.fields['customers'].queryset = get_customers_qs(company=user.company)

    class Meta:
        model = Project
        fields = '__all__'
        widgets = {
            'project_manager': HiddenInput,
            'company': HiddenInput,
        }


class UpdateProjectForm(BSModalModelForm):

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)
        self.fields['customers'].queryset = get_customers_qs(company=user.company)

    class Meta:
        model = Project
        fields = ('name', 'description', 'start_date', 'real_end', 'estimated_end_date', 'customers')


class CreateProjectPhaseForm(BSModalModelForm):

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        self.project_id = kwargs.pop('project_id', None)
        super().__init__(*args, **kwargs)
        self.fields['phase_manager'].queryset = get_phase_manager_qs(company=user.company)

    class Meta:
        model = ProjectPhase
        fields = '__all__'
        widgets = {
            'project': HiddenInput,
            'start_date': DateTimeInput
        }
    
    def clean(self):
        cleaned_data = super().clean()
        start_date = cleaned_data.get('start_date')
        end_date = cleaned_data.get('estimated_end_date')
        
        if start_date and end_date:
            if start_date >= end_date:
                raise forms.ValidationError("Start date cannot be later than end date.")
    
    def clean_estimated_end_date(self):
        end_date = self.cleaned_data.get('estimated_end_date')
        p = Project.objects.get(pk=self.project_id)
        if p.estimated_end_date:  
            if end_date >= p.estimated_end_date:
                raise forms.ValidationError("End date cannot be later than end date of the project.")
        return end_date


class DetailIssueForm(BSModalModelForm):
    
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)
        
    class Meta:
        model = Issue
        fields = '__all__'    

class DetailRiskForm(BSModalModelForm):
    
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)
        
    class Meta:
        model = Risk
        fields = '__all__'   
    

class DetailTaskForm(BSModalModelForm):
    
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)
        
    class Meta:
        model = Task
        fields = '__all__'   

class UpdateProjectPhaseForm(BSModalModelForm):

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)
        self.fields['phase_manager'].queryset = get_phase_manager_qs(company=user.company)

    class Meta:
        model = ProjectPhase
        fields = ('name', 'description', 'start_date', 'real_end_date', 'estimated_end_date', 'phase_manager')
           

class CreateRiskForm(BSModalModelForm):

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)
        self.fields['risk_assignee'].queryset = (get_project_managers_qs(company=user.company) | get_phase_manager_qs(company=user.company)).distinct()

    class Meta:
        model = Risk
        fields = '__all__'
        widgets = {
            'project_phase': HiddenInput
        }
 
class CreateTaskFormPhase(BSModalModelForm):

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)
        self.fields['task_dev'].queryset = (get_project_managers_qs(company=user.company) | get_phase_manager_qs(company=user.company)).distinct()
        self.fields['task_check'].queryset = (get_project_managers_qs(company=user.company) | get_phase_manager_qs(company=user.company)).distinct()

    class Meta:
        model = Task
        fields = '__all__'
        widgets = {
            'project_phase': HiddenInput
        }              

class CreateProjectRiskForm(BSModalModelForm):

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        project_id = kwargs.pop('project_id', None)
        super().__init__(*args, **kwargs)
        self.fields['risk_assignee'].queryset = (get_project_managers_qs(company=user.company) | get_phase_manager_qs(company=user.company)).distinct()
        self.fields['project_phase'].queryset = ProjectPhase.objects.filter(project=project_id)
    class Meta:
        model = Risk
        fields = '__all__'


class UpdateRiskForm(BSModalModelForm):

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)
        self.fields['risk_assignee'].queryset = (get_project_managers_qs(company=user.company) | get_phase_manager_qs(company=user.company)).distinct()

    class Meta:
        model = Risk
        #fields = ('risk_assignee','impact','state','probability','name','description','threat','starter','reactions','comment','start_date','state_change_date','reaction_date')
        exclude = ('project_phase',)
        

class CreateIssueForm(BSModalModelForm):

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)
        self.fields['issue_dev'].queryset = (get_project_managers_qs(company=user.company) | get_phase_manager_qs(company=user.company)).distinct()
        self.fields['issue_check'].queryset = (get_project_managers_qs(company=user.company) | get_phase_manager_qs(company=user.company)).distinct()

    class Meta:
        model = Issue
        fields = '__all__'
        widgets = {
            'project': HiddenInput
        }

class UpdateIssueForm(BSModalModelForm):
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)
        self.fields['issue_dev'].queryset = (get_project_managers_qs(company=user.company) | get_phase_manager_qs(company=user.company)).distinct()
        self.fields['issue_check'].queryset = (get_project_managers_qs(company=user.company) | get_phase_manager_qs(company=user.company)).distinct()

    class Meta:
        model = Issue
        fields = ('name' ,'issuestate', 'issue_dev', 'issue_check', 'description') 


  

class CreateTaskForm(BSModalModelForm):

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        project_id = kwargs.pop('project_id', None)
        super().__init__(*args, **kwargs)
        self.fields['task_dev'].queryset = (get_project_managers_qs(company=user.company) | get_phase_manager_qs(company=user.company)).distinct()
        self.fields['task_check'].queryset = (get_project_managers_qs(company=user.company) | get_phase_manager_qs(company=user.company)).distinct()
        self.fields['project_phase'].queryset = ProjectPhase.objects.filter(project=project_id)

    class Meta:
        model = Task
        fields = '__all__'     
        

class UpdateTaskForm(BSModalModelForm):

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        project_id = kwargs.pop('project_id', None)
        super().__init__(*args, **kwargs)
        self.fields['task_dev'].queryset = (get_project_managers_qs(company=user.company) | get_phase_manager_qs(company=user.company)).distinct()
        self.fields['task_check'].queryset = (get_project_managers_qs(company=user.company) | get_phase_manager_qs(company=user.company)).distinct()
        #self.fields['project_phase'].queryset = ProjectPhase.objects.filter(project=project_id)

    class Meta:
        model = Task
        fields = '__all__'
        #exclude = ('name', 'issueID', 'issuerstate', 'issue_dev', 'issue_check', 'description')


class LFAUpdateForm(BSModalModelForm):

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)
        #self.fields['customers'].queryset = get_customers_qs(company=user.company)

    class Meta:
        model = Project
        fields = ('g1', 'g2', 'g3', 'g4', 'p1', 'p2', 'p3', 'p4', 'o1', 'o2', 'o3', 'o4', 'i1', 'i2', 'i3', 'i4')


