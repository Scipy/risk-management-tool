from django.conf import settings
from django.db import models
from django.shortcuts import reverse
from django.utils.translation import gettext_lazy as _


class ActiveQuerySet(models.QuerySet):
    def active(self):
        return self.filter(is_active=True)

    def inactive(self):
        return self.filter(is_active=False)

    def delete(self):
        self.update(is_active=False)


class ActiveManager(models.Manager):
    def get_queryset(self):
        return ActiveQuerySet(self.model, using=self._db)


class ActiveModel(models.Model):
    is_active = models.BooleanField(default=True, editable=False)

    objects = ActiveManager()

    class Meta:
        abstract = True

    def delete(self):
        self.is_active = False
        self.save()


class Company(models.Model):
    name = models.CharField(max_length=255)
    logo = models.BinaryField()
    description = models.CharField(max_length=255, null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Companies'

    def __str__(self):
        return self.name

class LFA(ActiveModel):
    g1 = models.TextField('G1',  default="Goal")
    g2 = models.TextField('G2',  default="Why")
    g3 = models.TextField('G3',  default="What")
    g4 = models.TextField('G4',  default="How")

    p1 = models.TextField('P1',  default="Purpose")
    p2 = models.TextField('P2',  default="Why")
    p3 = models.TextField('P3',  default="What")
    p4 = models.TextField('P4',  default="How")

    o1 = models.TextField('O1',  default="Outcomes")
    o2 = models.TextField('O2',  default="Why")
    o3 = models.TextField('O3',  default="What")
    o4 = models.TextField('O4',  default="How")

    i1 = models.TextField('I1',  default="Inputs")
    i2 = models.TextField('I2',  default="Why")
    i3 = models.TextField('I3',  default="What")
    i4 = models.TextField('I4',  default="How")

    def __str__(self):
        return self.name

class Issue(models.Model):
    class IssueState(models.TextChoices):
        ASAP = 'ASAP', _('ASAP')
        HIGH_PRIORITY = 'HIGH-PRIORITY', _('HIGH PRIORITY')
        OPEN = 'OPEN', _('OPEN')
        DONE = 'CLOSED', _('CLOSED')

    name = models.CharField(max_length=20)
    issueID = models.DecimalField(max_digits=10, decimal_places=0)
    project = models.ForeignKey('Project', on_delete=models.CASCADE)

    issuestate = models.CharField(max_length=20, choices=IssueState.choices, default=IssueState.OPEN)
    issue_dev = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name ='issue_dev_ASSIGNED', null=True)
    issue_check = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='issue_check_CHECKED', null=True)
    description = models.CharField(max_length=255)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['project', 'issueID'], name='unique_issue')
        ]

    def get_absolute_url(self):
        return reverse('project-issues', kwargs={'pk': self.project.pk})

    def __str__(self):
        return self.name


class Task(models.Model):
    class TaskState(models.TextChoices):
        NOT_ASSIGNED = 'NOT_ASSIGNED', _('NOT ASSIGNED')
        ASSIGNED = 'ASSIGNED', _('ASSIGNED')
        DONE_NOT_CHECKED = 'DONE_NOT_CHECKED', _('DONE NOT CHECKED')
        DONE_AND_CHECKED = 'DONE_AND_CHECKED', _('DONE AND CHECKED')

    name = models.CharField(max_length=20)
    taskID = models.DecimalField(max_digits=10, decimal_places=0)
    add_date = models.DateField(auto_now_add=True)
    final_date = models.DateField()
    real_final_date = models.DateField(null=True, blank=True)
    taskstate = models.CharField(max_length=20, choices=TaskState.choices, default=TaskState.NOT_ASSIGNED)
    task_dev = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name ='task_dev_ASSIGNED', null=True)
    task_check = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='task_check_CHECKED', null=True)
    description = models.CharField(max_length=255)
    project_phase = models.ForeignKey('ProjectPhase', on_delete=models.CASCADE, related_name="tasks")

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['project_phase', 'taskID'], name='unique_task')
        ]
    
    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('tasks-detail', kwargs={'project_id': self.project_phase.project.pk, 'project_phase': self.project_phase.pk, 'pk': self.pk})
    


class Risk(models.Model):

    class State(models.TextChoices):
        ACTIVE = 'Active', _('Active')
        CLOSED = 'Closed', _('Closed')
        HAPPENED = 'Happened', _('Happened')

    class Impact(models.TextChoices):
        VVD = 'VHI', _('Very high impact')
        VD = 'HI', _('High impact')
        SD = 'MI', _('Middle impact')
        MD = 'SI', _('Small impact')
        VMD = 'VSI', _('Very small impact')

    class Probability(models.TextChoices):
        VVP = 'VHP', _('Very high probability')
        VP = 'HP', _('High probability')
        SP = 'MP', _('Middle probability')
        NP = 'SP', _('Small probability')
        VNP = 'VSP', _('Very small probability')

    risk_assignee = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    project_phase = models.ForeignKey('ProjectPhase', on_delete=models.CASCADE)
    risk_type = models.ForeignKey('RiskType', on_delete=models.CASCADE, null=True, blank=True)
    impact = models.CharField(max_length=50, choices=Impact.choices, default=Impact.VMD)
    state = models.CharField(max_length=50, choices=State.choices, default=State.HAPPENED)
    probability = models.CharField(max_length=50, choices=Probability.choices, default=Probability.VNP)

    name = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    threat = models.TextField()
    starter = models.TextField()
    reactions = models.TextField(null=True, blank=True)
    comment = models.TextField(null=True, blank=True)
    start_date = models.DateField()
    state_change_date = models.DateField(null=True, blank=True)
    reaction_date = models.DateField(null=True, blank=True)

    @property
    def risk(self):
        matrix = [
            ['VHRV', 'VHRV', 'HRV', 'HRV', 'MRV'],
            ['VHRV', 'VHRV', 'HRV', 'MRV', 'SRV'],
            ['HRV', 'HRV', 'MRV', 'SRV', 'SRV'],
            ['HRV', 'MRV', 'SRV', 'VSRV', 'VSRV'],
            ['MRV', 'SRV', 'SRV', 'VSRV', 'VSRV']
        ]

        proba_to_id = dict(zip(map(lambda x: x[0], self.Probability.choices), range(0, len(self.Probability))))
        impacts_to_id = dict(zip(map(lambda x: x[0], self.Impact.choices), range(0, len(self.Impact))))

        return matrix[proba_to_id[self.probability]][impacts_to_id[self.impact]]

    def get_absolute_url(self):
        return reverse('risk-detail', kwargs={'project_id': self.project_phase.project.pk, 'phase_id': self.project_phase.pk, 'pk': self.pk})


class Project(ActiveModel):
    project_manager = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='projects')
    company = models.ForeignKey('Company', on_delete=models.CASCADE)
    customers = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True)

    name = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    start_date = models.DateField()
    real_end = models.DateField(null=True, blank=True)
    estimated_end_date = models.DateField(null=True, blank=True)


    g1 = models.TextField('G1',  default="Goal")
    g2 = models.TextField('G2',  default="Why")
    g3 = models.TextField('G3',  default="What")
    g4 = models.TextField('G4',  default="How")

    p1 = models.TextField('P1',  default="Purpose")
    p2 = models.TextField('P2',  default="Why")
    p3 = models.TextField('P3',  default="What")
    p4 = models.TextField('P4',  default="How")

    o1 = models.TextField('O1',  default="Outcomes")
    o2 = models.TextField('O2',  default="Why")
    o3 = models.TextField('O3',  default="What")
    o4 = models.TextField('O4',  default="How")

    i1 = models.TextField('I1',  default="Inputs")
    i2 = models.TextField('I2',  default="Why")
    i3 = models.TextField('I3',  default="What")
    i4 = models.TextField('I4',  default="How")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('project-detail', kwargs={'pk': self.pk})


class RiskType(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class RiskSubtype(models.Model):
    risk_type = models.ForeignKey('RiskType', on_delete=models.CASCADE)
    name = models.CharField(max_length=255)


class ProjectPhase(ActiveModel):
    name = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    start_date = models.DateField()
    real_end_date = models.DateField(null=True, blank=True)
    estimated_end_date = models.DateField(null=True, blank=True)

    phase_manager = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    project = models.ForeignKey('Project', on_delete=models.CASCADE)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['project', 'name'], name='unique_projectphase')
        ]

    def get_absolute_url(self):
        return reverse('project-phase-detail', kwargs={'project_id': self.project.pk, 'pk': self.pk})

    def __str__(self):
        return self.name
