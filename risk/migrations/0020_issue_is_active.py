# Generated by Django 3.1.7 on 2022-05-09 15:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('risk', '0019_auto_20220508_2348'),
    ]

    operations = [
        migrations.AddField(
            model_name='issue',
            name='is_active',
            field=models.BooleanField(default=True, editable=False),
        ),
    ]
