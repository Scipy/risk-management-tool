# Generated by Django 3.1.7 on 2022-04-30 14:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('risk', '0015_auto_20220426_1521'),
    ]

    operations = [
        migrations.AlterField(
            model_name='issue',
            name='issuestate',
            field=models.CharField(choices=[('ASAP', 'ASAP'), ('HIGH-PRIORITY', 'HIGH PRIORITY'), ('OPEN', 'OPEN'), ('CLOSED', 'CLOSED')], default='OPEN', max_length=20),
        ),
    ]
