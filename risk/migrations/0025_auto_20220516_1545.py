# Generated by Django 3.1.7 on 2022-05-16 15:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('risk', '0024_auto_20220515_1813'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='g1',
            field=models.TextField(default='Goal', verbose_name='G1'),
        ),
        migrations.AddField(
            model_name='project',
            name='g2',
            field=models.TextField(default='Why', verbose_name='G2'),
        ),
        migrations.AddField(
            model_name='project',
            name='g3',
            field=models.TextField(default='What', verbose_name='G3'),
        ),
        migrations.AddField(
            model_name='project',
            name='g4',
            field=models.TextField(default='How', verbose_name='G4'),
        ),
        migrations.AddField(
            model_name='project',
            name='i1',
            field=models.TextField(default='Inputs', verbose_name='I1'),
        ),
        migrations.AddField(
            model_name='project',
            name='i2',
            field=models.TextField(default='Why', verbose_name='I2'),
        ),
        migrations.AddField(
            model_name='project',
            name='i3',
            field=models.TextField(default='What', verbose_name='I3'),
        ),
        migrations.AddField(
            model_name='project',
            name='i4',
            field=models.TextField(default='How', verbose_name='I4'),
        ),
        migrations.AddField(
            model_name='project',
            name='o1',
            field=models.TextField(default='Outcomes', verbose_name='O1'),
        ),
        migrations.AddField(
            model_name='project',
            name='o2',
            field=models.TextField(default='Why', verbose_name='O2'),
        ),
        migrations.AddField(
            model_name='project',
            name='o3',
            field=models.TextField(default='What', verbose_name='O3'),
        ),
        migrations.AddField(
            model_name='project',
            name='o4',
            field=models.TextField(default='How', verbose_name='O4'),
        ),
        migrations.AddField(
            model_name='project',
            name='p1',
            field=models.TextField(default='Purpose', verbose_name='P1'),
        ),
        migrations.AddField(
            model_name='project',
            name='p2',
            field=models.TextField(default='Why', verbose_name='P2'),
        ),
        migrations.AddField(
            model_name='project',
            name='p3',
            field=models.TextField(default='What', verbose_name='P3'),
        ),
        migrations.AddField(
            model_name='project',
            name='p4',
            field=models.TextField(default='How', verbose_name='P4'),
        ),
    ]
