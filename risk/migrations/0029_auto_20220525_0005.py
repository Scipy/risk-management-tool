# Generated by Django 3.1.7 on 2022-05-25 00:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('risk', '0028_auto_20220524_1857'),
    ]

    operations = [
        migrations.CreateModel(
            name='RiskType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.AlterField(
            model_name='risk',
            name='impact',
            field=models.CharField(choices=[('VVD', 'Very high impact'), ('VD', 'High impact'), ('SD', 'Middle impact'), ('ND', 'Small impact'), ('VND', 'Very small impact')], default='VND', max_length=50),
        ),
        migrations.AlterField(
            model_name='risk',
            name='probability',
            field=models.CharField(choices=[('VVP', 'Very high probability'), ('VP', 'High probability'), ('SP', 'Middle probability'), ('NP', 'Small probability'), ('VNP', 'Very small probability')], default='VNP', max_length=50),
        ),
        migrations.CreateModel(
            name='RiskSubtype',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('risk_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='risk.risktype')),
            ],
        ),
        migrations.AddField(
            model_name='risk',
            name='risk_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='risk.risktype'),
        ),
    ]
