describe('Logging In - CSRF Tokens', function () {
  const username = 'PM1'
  const password = 'Superpassword1'

Cypress.Commands.add('loginByCSRF', (csrfToken) => {
    cy.request({
      method: 'POST',
      url: 'localhost:8000',
      failOnStatusCode: false, // dont fail so we can make assertions
      form: true, // we are submitting a regular form body
      body: {
        username,
        password,
      },
    })
  })

beforeEach(() => {
    cy.visit('localhost:8000')
    cy.clearCookies()
})

it('Go to landing page',() => {

    cy.visit('localhost:8000')
    cy.contains('Support of Risk Management in Agile Product Development')
      .should('be.visible')

});

it('Test of existing all parts of landingpage', () => {
    
    cy.contains('Username:')
      .should('be.visible')
    cy.contains('Password:') 
      .should('be.visible')
    cy.get('input[name=username]')
      .invoke('attr','placeholder')
      .should('contain', 'User Name')
    cy.get('input[type=password]')
      .invoke('attr','placeholder')
      .should('contain', 'Password') 
    cy.get('.text-dark')
      .should('contain','For sign up, please contact system admin via riskmanagementtooldp@gmail.com')
    cy.get('input[type=submit]')
      .invoke('attr','value')
      .should('contain','Login')     
});

it('Try to login with incorect login data', () => {
    
    cy.get('input[name=username]')
      .clear()
      .type('wrong')  
    cy.get('input[type=password]')
      .clear()
      .type('fakepassword')    
    cy.get('input[type=submit]')
      .click()
});


})
