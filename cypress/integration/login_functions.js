/// <reference types="cypress" />

describe('Logging In - CSRF Tokens', function () {
    const username = 'PM1'
    const password = 'Superpassword1'
  
    Cypress.Commands.add('loginByCSRF', (csrfToken) => {
      cy.request({
        method: 'POST',
        url: 'localhost:8000',
        failOnStatusCode: false,
        form: true, 
        body: {
          username,
          password,
          _csrf: csrfToken, 
        },
      })
    })

    before('TokenViaToken', function () {
      cy.request('localhost:8000')
      .its('body')
      .then((body) => {
        const $html = Cypress.$(body)
        const csrf = $html.find('input[name=_csrf]').val()
        cy.loginByCSRF(csrf)
      })
    })
  
    beforeEach(function () {
      cy.visit('localhost:8000')
      cy.viewport(500, 380)
      cy.get('input[name=username]').type(username)
      cy.get('input[name=password]').type(password)
      cy.contains('Login')
        .click() 
    })
  
    it('redirects to login', () => {
      cy.visit('localhost:8000')
    })
  
    it('403 status without a valid CSRF token', function () {
      cy.loginByCSRF('invalid-token')
      .its('status')
      .should('eq', 403)
    })



  it('Go to project', () => {
    cy.contains('Tkanicky') 
      .click()
    cy.contains('Project details')
      .should('be.visible')  
    cy.contains('Tkanicky')
      .should('be.visible') 
    cy.contains('Phases')
      .should('be.visible')      
  });

  it('Try menu', () => {
    cy.contains('Tkanicky') 
      .click()
    cy.contains('LFA') 
      .click({force: true})
    cy.contains('Objectives')
      .should('be.visible')
    cy.contains('Success Measures')
      .should('be.visible')
    cy.contains('Verification')
      .should('be.visible')
    cy.contains('Assumptions')
      .should('be.visible')  
    cy.contains('Time') 
      .click({force: true})
    cy.get('div[class=accordion]')
    cy.contains('Tasks') 
      .click({force: true})
    cy.contains('Add Task')
      .should('be.visible')    
    cy.contains('Issues') 
      .click({force: true})
    cy.contains('Add Issue')
      .should('be.visible') 
    cy.contains('Risk') 
      .click({force: true})
    cy.contains('Create')
      .should('be.visible')   
    cy.contains('Company') 
      .click({force: true})
    cy.contains('Project')  
  });

  it('Try project forms', () => {
    cy.contains('Tkanicky') 
      .click()
    cy.contains('Edit') 
      .click() 
    cy.get('div[class=modal-header')
      .should('be.visible')
    cy.get('button[class=close')
      .click()     
  })
})

 