// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

Cypress.Commands.add('login', (username, password) => {

    return cy.request({
      url: '',
      method: 'HEAD' // cookies are in the HTTP headers, so HEAD suffices
    }).then(() => {
  
      cy.getCookie('sessionid').should('not.exist')
      cy.getCookie('csrftoken').its('value').then((token) => {
        let oldToken = token
        cy.request({
          url: '/login/',
          method: 'POST',
          form: true,
          followRedirect: false, // no need to retrieve the page after login
          body: {
            username: PM1,
            password: Superpassword1,
            csrfmiddlewaretoken: ybH7uzIU2Na9aj8ZLmXwA9N5zJOJpBmcaLuR0RdT3KntqGk7r8lRt5R8dGgWFiEy
          }
        }).then(() => {
  
          cy.getCookie('sessionid').should('exist')
          return cy.getCookie('csrftoken').its('value')
  
        })
      })
    })
  
  })